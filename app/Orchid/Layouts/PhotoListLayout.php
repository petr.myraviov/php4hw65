<?php

namespace App\Orchid\Layouts;

use App\Photo;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PhotoListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'photos';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('title','Title')
                ->sort()
                ->filter(TD::FILTER_DATE)->render(function (Photo $photo){
                return Link::make($photo->photo)
                    ->route('platform.photos.edit', $photo);
            }),
            TD::set('created_at', 'Created')->sort(),
            TD::set('updated_at', 'Last edit')->sort(),
            TD::set('false', 'null')->sort()
        ];
    }
}
