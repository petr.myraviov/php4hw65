<?php

namespace App\Orchid\Screens;

use App\Comment;
use App\Orchid\Layouts\CommentListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class CommentListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'All Comments';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All Comments description';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'comment' => Comment::filters()->defaultSort('id')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new comment')
            ->icon('icon-pencil')
            ->route('platform.comments.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            CommentListLayout::class
        ];
    }
}
