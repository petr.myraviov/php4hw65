<?php

namespace App\Orchid\Screens;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PhotoEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Creat Photo';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Manage photo';
    public $exists = false;

    /**
     * Query data.
     *
     * @param Photo $photo
     * @return array
     */
    public function query(Photo $photo): array
    {
        $this->exists = $photo->exists;
        if ($this->exists){
            $this->name = "edit photo";
        }
        $photo->load('attachment');
        return [
            'photo' => $photo
        ];
    }

    /**d
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create photo')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update photo')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Cropper::make('photo.photo')
                ->title('Large web banner image, generally in the from and center')
                ->width(400)
                ->height(400),
                Relation::make('User')
                    ->title('User')
                    ->fromModel(User::class, 'name', 'id'),
            ])
        ];
    }

    /**
     * @param Photo $photo
     * @param PhotoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Photo $photo, PhotoRequest $request)
    {
        $photo->fill($request->get('photo'))->save();
        $photo->attachment()->syncWithoutDetaching(
            $request->input('photo.attachment',[]));
        Alert::info('You has successfully created post');
        return redirect()->route('platform.photos.list');
    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Photo $photo)
    {
        $photo->delete()
        ? Alert::info('Delete')
        : Alert::warning('No delete!');
        return redirect()->route('platform.photos.list');
    }
}
