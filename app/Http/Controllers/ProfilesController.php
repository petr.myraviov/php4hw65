<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

class ProfilesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
        return view('profiles.index'
            , ['photos' => Photo::all(),
               $user
                ]
        );
    }


    /**
     * @param PhotoRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PhotoRequest $request, User $user)
    {
        $photo = new Photo();
        $file = $request->file('photo');
        if (!is_null($file)){
            $path = $file->store('photo', 'public');
            $photo['photo'] = $path;
        }
        $photo->user_id = $request->user()->id;
        $photo->save();
        return redirect()->route('index');
    }

    /**
     * @param Photo $photo
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect()->route('index');
    }

    public function actionLike()
    {

    }

}
