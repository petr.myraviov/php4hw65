<?php

namespace App\Http\Controllers;
use App\Like;
use App\Photo;
use App\User;
use http\Client\Response;
use Illuminate\Http\Request;

class LakesControllers extends Controller
{
    /**
     * @param Request $request
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createLike(Request $request, Photo $photo)
    {
        $like = new Like();
        $like->number = $request->input('number');
        $like->photo_id = $photo->id;
        $like->user_id = $request->user()->id;
        $like->save();
        return redirect()->route('tape');
    }

}
