<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;

class PhotoCommentsController extends Controller
{

    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(CommentRequest $request, Photo $photo)
    {
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->photo_id = $photo->id;
        $comment->user_id = $request->user()->id;
        $comment->save();
        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 201);
    }

    /**
     * @param CommentRequest $request
     * @param int $comment_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(CommentRequest $request, int $comment_id)
    {
        $comment =Comment::finOrFail($comment_id);
        $comment->update($request->all());
        return   response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 201);
    }


    /**
     * @param int $photo_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $photo_id, $id )
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return redirect()->route('tape');
    }

}
