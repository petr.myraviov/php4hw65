<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SubscriptionsController extends Controller
{

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSubscription(Request $request, User $user)
    {
        $user =  User::findOrFail($user);
        $subscription = $request->user();
        $subscriber = $request->user();
        $user = $request->user();
        $user->subscribers()->attach($subscriber->id);
        $user->subscriptions()->attach($subscription->id);
        return redirect()->route('tape');
    }

}
