<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text($maxNbChars = 200),
        'photo_id' => rand(1,8),
        'user_id' => rand(1,5),
    ];
});
