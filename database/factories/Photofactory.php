<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Photo;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @param int $photo_number
 * @return string
 */
function photo_path(int $photo_number):string
{
    $path = storage_path() ."/seed_photos/" . "$photo_number" . ".jpg";
    $photo_name = md5($path). ".jpg";
    $resize = Image::make($path)->fit(300)->encode('jpg');
    Storage::disk('public')->put('photo/'.$photo_name, $resize->__toString());
    return 'photo/'. $photo_name;
}
$factory->define(Photo::class, function (Faker $faker) {
    return [
        'photo' => photo_path(rand(1,8)),
        'user_id' => rand(1,5),
    ];
});
