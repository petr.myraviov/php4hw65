<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = factory(\App\User::class)->create();
        $user_2 = factory(\App\User::class)->create();
        $user_3 = factory(\App\User::class)->create();
        factory(\App\User::class,5)->create();
        $user_1->subscribers()->attach([rand(1,5),rand(1,5)]);
        $user_2->subscribers()->attach([rand(1,5),rand(1,5)]);
        $user_3->subscribers()->attach([rand(1,5),rand(1,5)]);
    }
}
