<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index')->name('home');
Route::get('profiles/index', 'ProfilesController@index')->name('index');
Route::post('profiles/index', 'ProfilesController@store')->name('storePhoto');
Route::delete('profiles/index/{photo}', 'ProfilesController@destroy')->name('destroyPhoto');
Route::get('profiles/tape', 'TapeController@tape')->name('tape');
Route::resource('photos.comments', 'PhotoCommentsController')->only(['store','update', 'destroy']);
Route::post('profiles/tape', 'SubscriptionsController@storeSubscription')->name('storeSubscription');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
