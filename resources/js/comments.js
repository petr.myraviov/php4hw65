$(document).ready(function () {
    $('.create-comment-btn').click(function (event) {
        event.preventDefault();
        console.log(event.preventDefault());
        const id = $(this).attr('data-photo-id');
        const data = $(`#create-comment-${id}`).serialize();
        const photoId = $(`#photo_id-${id}`).val();
        $.ajax({
            url: `/photos/${photoId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (msg) {
                console.log(msg.comment);
                renderData(msg.comment);
            })
            .fail(function (response) {
                console.log('FAIL RESPONSE =================> ', response);
                let errors = response.responseJSON.errors;
                renderErrors(errors);
            });
    });
    function clearForm() {
        $('.create-comment').trigger('reset')
    }
    function renderData(html) {
        let commentsBlock = $('.comments-block');
        $(commentsBlock).append(html);
        clearForm();
    }

    function renderErrors(errors) {
        let keys = Object.keys(errors);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            let element = $(`#${key}`);
            if (!element.hasClass('is-invalid')) {
                $(element).addClass('is-invalid');

                let html = `<ul class="errors-${key}">`;
                for (let j = 0; j < errors[key].length; j++) {
                    html += `<li>${errors[key][j]}</li>`
                }
                html += '</ul>';
                $(element).parent().append(html);
            }
        }
    }

    $('#body').on('change', function (event) {
        if ($(this).hasClass('is-invalid')){
            $('.errors-body').remove();
            $('#body').removeClass('is-invalid');
        }
    });
});
