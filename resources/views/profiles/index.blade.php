@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form style="padding-bottom: 40px" enctype="multipart/form-data" method="post" action="{{route('storePhoto')}}">
        @csrf
        <div class="input-group is-invalid">
            <div class="custom-file">
                <input  name="photo" type="file" class="custom-file-input" id="validatedInputGroupCustomFile" required>
                <label class="custom-file-label" for="validatedInputGroupCustomFile">Загрузить фото...</label>
            </div>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Загрузить</button>
            </div>
        </div>
    </form>

    <div class="container">
        <div class="row">
        @foreach(Auth::user()->photos as $photo)
                <div class="modal fade" id="staticBackdrop-{{$photo->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">{{$photo->user->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <span class="likebtn-wrapper" data-theme="large" data-lang="ru" data-identifier="{{$photo->user->id}}"></span>
                                <div id="comments-block" class="col-8 scrollit">
                                    @foreach($photo->comments as $comment)
                                        <x-comment :comment="$comment"></x-comment>
                                    @endforeach
                                </div>
                            </div>
                            <div class="modal-footer">
                                <form method="post" action="{{route('destroyPhoto', ['photo' => $photo])}}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="active-product-area">Удалить фото</button>
                                </form>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Understood</button>
                            </div>
                        </div>
                    </div>
                </div>
                @csrf
                <div style="margin: auto" class="col-xl-4 col-md-4 col-lg-4">
                    <div  style="border-radius: 300px; height: 300px; width: 300px"  class="thumb">
                        <button type="button" style="border-radius: 300px; height: 300px; width: 300px;" data-toggle="modal" data-target="#staticBackdrop-{{$photo->id}}">
                            <img src="{{asset('storage/'.$photo->photo)}}" class="card-img" style="border-radius: 300px; height: 300px; width: 300px; margin-left: -6px; margin-top: -2px"  alt="{{$photo->id}}">
                        </button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @endsection
