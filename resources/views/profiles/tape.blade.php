@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="container">
    <div class="row">
        @foreach($photos as $photo)
        @csrf
        <div class="modal fade" id="staticBackdrop-{{$photo->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">{{$photo->user->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="likebtn-wrapper" data-theme="large" data-lang="ru" data-identifier="{{$photo->user->id}}"></span>
                        <div id="comments-block" class="col-8 scrollit comments-block">
                            @foreach($photo->comments as $comment)
                                <x-comment :comment="$comment"></x-comment>
                            @endforeach
                        </div>
                        <div class="col-4">
                            <div class="comment-form">
                                <form id="create-comment-{{$photo->id}}" class="create-comment">
                                    @csrf
                                    <input type="hidden" id="photo_id-{{$photo->id}}" value="{{$photo->id}}">
                                    <div class="form-group">
                                        <label for="body">Комментарий</label>
                                        <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                                    </div>
                                    <button  id="create-comment-btn" data-photo-id="{{$photo->id}}" type="submit" class="btn btn-outline-primary btn-sm btn-block create-comment-btn">Добавьте новый
                                        комментарий
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Understood</button>
                    </div>
                </div>
            </div>
        </div>
            @csrf
            <div style="margin: auto;padding-top: 20px" class="col-xl-4 col-md-4 col-lg-4">
                    <div  style="border-radius: 300px; height: 300px; width: 300px"  class="thumb">
                        <button type="button" style="border-radius: 300px; height: 300px; width: 300px;" data-toggle="modal" data-target="#staticBackdrop-{{$photo->id}}">
                            <img src="{{asset('storage/'.$photo->photo)}}" class="card-img" style="border-radius: 300px; height: 300px; width: 300px; margin-left: -6px; margin-top: -2px"  alt="{{$photo->id}}">
                        </button>
                    </div>
                </div>
        @endforeach
    </div>
</div>
@foreach($users as $user)
    <form method="post" action="{{route('storeSubscription')}}">
            @csrf
        <input type="hidden" name="subscriber_id" value="{{$user->id}}" id="validatedInputGroupCustomFile" >{{$user->name}}
{{--        <input  type="hidden" style="margin-top: 100px"  name="subscription_id" value="{{$user->id}}" id="validatedInputGroupCustomFile" >{{$user->name}}--}}
        <button class="btn btn-outline-secondary" type="submit">Подписка</button>
    </form>
@endforeach
@endsection

<script>
    (function(d,e,s){
        if(d.getElementById("likebtn_wjs"))
        return;
        a=d.createElement(e);
        m=d.getElementsByTagName(e)[0];
        a.async=1;
        a.id="likebtn_wjs";
        a.src=s;
        m.parentNode.insertBefore(a, m)
    })
    (document,"script","//w.likebtn.com/js/w/widget.js");
</script>
